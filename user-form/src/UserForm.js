import React, { useState } from 'react';

const UserForm = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [employed, setEmployed] = useState(false);
    const [favoriteColor, setFavoriteColor] = useState('#000000');
    const [ketchup, setKetchup] = useState(false);
    const [mustard, setMustard] = useState(false);
    const [mayonnaise, setMayonnaise] = useState(false);
    const [guacamole, setGuacamole] = useState(false);
    const [stooge, setStooge] = useState('Larry');
    const [notes, setNotes] = useState('');
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [formValid, setFormValid] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        setFormSubmitted(true);
        if (validateForm()) {
            const formData = {
                firstName,
                lastName,
                age,
                employed,
                favoriteColor,
                sauces: { ketchup, mustard, mayonnaise, guacamole },
                stooge,
                notes,
            };
            alert(JSON.stringify(formData, null, 2));
        }
    };

    const validateForm = () => {
        const isValidFirstName = /^[A-Za-z\s]+$/.test(firstName);
        const isValidLastName = /^[A-Za-z\s]+$/.test(lastName);
        const isValidAge = /^[0-9]+$/.test(age);
        const isValidNotes = notes.length <= 100;

        setFormValid(isValidFirstName && isValidLastName && isValidAge && isValidNotes);

        return isValidFirstName && isValidLastName && isValidAge && isValidNotes;
    };


    const handleReset = () => {
        setFirstName('');
        setLastName('');
        setAge('');
        setEmployed(false);
        setFavoriteColor('#000000');
        setKetchup(false);
        setMustard(false);
        setMayonnaise(false);
        setGuacamole(false);
        setStooge('Larry');
        setNotes('');
        setFormSubmitted(false);
        setFormValid(false);
    };

    const handleChangeFirstName = (e) => {
        setFirstName(e.target.value);
        setFormSubmitted(false);
    };

    const handleChangeLastName = (e) => {
        setLastName(e.target.value);
        setFormSubmitted(false);
    };

    const handleChangeAge = (e) => {
        setAge(e.target.value);
        setFormSubmitted(false);
    };

    const handleChangeEmployed = (e) => {
        setEmployed(e.target.checked);
        setFormSubmitted(false);
    };

    const handleChangeFavoriteColor = (e) => {
        setFavoriteColor(e.target.value);
        setFormSubmitted(false);
    };

    const handleChangeSauce = (sauce, value) => {
        switch (sauce) {
            case 'ketchup':
                setKetchup(value);
                break;
            case 'mustard':
                setMustard(value);
                break;
            case 'mayonnaise':
                setMayonnaise(value);
                break;
            case 'guacamole':
                setGuacamole(value);
                break;
            default:
                break;
        }
        setFormSubmitted(false);
    };

    const handleChangeStooge = (e) => {
        setStooge(e.target.value);
        setFormSubmitted(false);
    };

    const handleChangeNotes = (e) => {
        setNotes(e.target.value);
        setFormSubmitted(false);
    };

    const formState = {
        stooge,
        employed,
    };

    if (firstName !== '') {
        formState.firstName = firstName;
    }
    if (lastName !== '') {
        formState.lastName = lastName;
    }
    if (age !== '') {
        formState.age = age;
    }
    if (favoriteColor !== '#000000') {
        formState.favoriteColor = favoriteColor;
    }
    if (ketchup) {
        formState.ketchup = ketchup;
    }
    if (mustard) {
        formState.mustard = mustard;
    }
    if (mayonnaise) {
        formState.mayonnaise = mayonnaise;
    }
    if (guacamole) {
        formState.guacamole = guacamole;
    }
    if (notes !== '') {
        formState.notes = notes;
    }


    const formHasChanges =
        firstName !== '' ||
        lastName !== '' ||
        age !== '' ||
        employed !== false ||
        favoriteColor !== '#000000' ||
        ketchup !== false ||
        mustard !== false ||
        mayonnaise !== false ||
        guacamole !== false ||
        stooge !== 'Larry' ||
        notes !== '';

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="firstName">Firstname:</label>
                <input
                    type="text"
                    id="firstName"
                    value={firstName}
                    onChange={handleChangeFirstName}
                    className={(formSubmitted || formValid) && !/^[A-Za-z\s]+$/.test(firstName) ? 'invalid' : ''}
                />
            </div>

            <div>
                <label htmlFor="lastName">Lastname:</label>
                <input
                    type="text"
                    id="lastName"
                    value={lastName}
                    onChange={handleChangeLastName}
                    className={(formSubmitted || formValid) && !/^[A-Za-z\s]+$/.test(lastName) ? 'invalid' : ''}
                />
            </div>

            <div>
                <label htmlFor="age">Age:</label>
                <input
                    type="text"
                    id="age"
                    value={age}
                    onChange={handleChangeAge}
                    className={(formSubmitted || formValid) && !/^[0-9]+$/.test(age) ? 'invalid' : ''}
                />
            </div>

            <div>
                <label htmlFor="employed">Employed:</label>
                <input
                    type="checkbox"
                    id="employed"
                    checked={employed}
                    onChange={handleChangeEmployed}
                />
            </div>

            <div>
                <label htmlFor="favoriteColor">Favorite color:</label>
                <input
                    type="color"
                    id="favoriteColor"
                    value={favoriteColor}
                    onChange={handleChangeFavoriteColor}
                />
            </div>

            <div className='field'>
                <label>Sauces:</label>
                <div>
                    <label htmlFor="ketchup">
                        <input
                            type="checkbox"
                            id="ketchup"
                            checked={ketchup}
                            onChange={(e) => handleChangeSauce('ketchup', e.target.checked)}
                        />
                        Ketchup
                    </label>
                </div>
                <div>
                    <label htmlFor="mustard">
                        <input
                            type="checkbox"
                            id="mustard"
                            checked={mustard}
                            onChange={(e) => handleChangeSauce('mustard', e.target.checked)}
                        />
                        Mustard
                    </label>
                </div>
                <div>
                    <label htmlFor="mayonnaise">
                        <input
                            type="checkbox"
                            id="mayonnaise"
                            checked={mayonnaise}
                            onChange={(e) => handleChangeSauce('mayonnaise', e.target.checked)}
                        />
                        Mayonnaise
                    </label>
                </div>
                <div>
                    <label htmlFor="guacamole">
                        <input
                            type="checkbox"
                            id="guacamole"
                            checked={guacamole}
                            onChange={(e) => handleChangeSauce('guacamole', e.target.checked)}
                        />
                        Guacamole
                    </label>
                </div>

            </div>

            <div>
                <label>Best Stooge:</label>

                <label htmlFor="larry">
                    <input
                        type="radio"
                        id="larry"
                        name="stooge"
                        value="Larry"
                        checked={stooge === 'Larry'}
                        onChange={handleChangeStooge}
                    />
                    Larry
                </label>

                <label htmlFor="moe">
                    <input
                        type="radio"
                        id="moe"
                        name="stooge"
                        value="Moe"
                        checked={stooge === 'Moe'}
                        onChange={handleChangeStooge}
                    />
                    Moe
                </label>

                <label htmlFor="curly">
                    <input
                        type="radio"
                        id="curly"
                        name="stooge"
                        value="Curly"
                        checked={stooge === 'Curly'}
                        onChange={handleChangeStooge}
                    />
                    Curly
                </label>
            </div>

            <div>
                <label htmlFor="notes">Notes:</label>
                <textarea
                    id="notes"
                    value={notes}
                    onChange={handleChangeNotes}
                    className={(formSubmitted || formValid) && notes.length > 100 ? 'invalid' : ''}
                />
            </div>

            <div>
                <button type="submit" disabled={!formHasChanges}>
                    Submit
                </button>
                <button type="button" onClick={handleReset} disabled={!formHasChanges}>
                    Reset
                </button>
            </div>
            <div>
                <pre>{JSON.stringify(formState, null, 2)}</pre>
            </div>
        </form>
    );
};

export default UserForm;
